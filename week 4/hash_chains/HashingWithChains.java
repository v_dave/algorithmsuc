import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;



/**
 *
 * @author Vaibhav
 */
public class HashingWithChains {
    static int m=0;
    public static void main(String[] args) throws IOException {
        BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
    //   BufferedReader read=new BufferedReader(new FileReader("D:\\Algorithms\\week 4\\hash_chains\\tests\\06"));
        BufferedWriter wr=new BufferedWriter(new OutputStreamWriter(System.out));
        
        
        
        
        
         m=Integer.parseInt(read.readLine());
         
        Map<Integer,ArrayList<String>> main = new HashMap<Integer, ArrayList<String>>();
        String input[]=null;
        String s="";
        int t=Integer.parseInt(read.readLine());
        
        
        for(int i=0;i<t;i++){
            s=read.readLine();
            input=s.split(" ");
            
            if(input[0].equals("check") ){
                
                 ArrayList<String> temp=new ArrayList<String>();
                 temp=main.get(Integer.parseInt(input[1]));
                 
                 if(temp!=null){
                 for(int j=temp.size()-1;j>0;j--){
                     System.out.print(temp.get(j)+" ");
                 }
                    try{ 
                    System.out.println(temp.get(0));
                    }
                    catch(Exception e){
                        System.out.println("");
                    }
                 }
                 else{
                     System.out.println("");
                 }
            }
            else{
                int position=0;
                if(input[0].equals("add")){
                    position=fun(input[1]);
                    if(main.get(position)==null){
                    main.put(position, new ArrayList<String>());
                    ArrayList<String> temp=new ArrayList<String>();
                    temp=main.get(position);
                    
                    if(!temp.contains(input[1]))
                    temp.add(input[1]);
                    main.put(position, temp);
                    }
                    
                    else{
                    ArrayList<String> temp=new ArrayList<String>();
                    temp=main.get(position);
                    if(!temp.contains(input[1]))
                    temp.add(input[1]);
                    main.put(position, temp);
                    }
                }
                else if(input[0].equals("find")){
                     position=fun(input[1]);
                    ArrayList<String> temp=new ArrayList<String>();
                    temp=main.get(position);
                    if(temp==null){
                        System.out.println("no");
                    }
                    else{
                        if(temp.contains(input[1])){
                            System.out.println("yes");
                        }
                        else{
                           System.out.println("no");
                        }
                    }
                
                }
                else if(input[0].equals("del")){
                    position=fun(input[1]);
                    ArrayList<String> temp=new ArrayList<String>();
                    temp=main.get(position);
                    if(temp==null){
                        ;
                    }
                    else{
                        temp.remove(input[1]);
                        main.put(position, temp);
                    }
            }
            
        }
        
    }
    }
    
    static int fun(String s) {
        long hash = 0;
        for (int i = s.length() - 1; i >= 0; --i)
            hash = (hash * 263 + s.charAt(i)) % 1000000007;
        return (int)hash % m;
    }
    
}
