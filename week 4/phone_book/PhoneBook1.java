/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.*;
import java.util.*;

/**
 *
 * @author Vaibhav
 */
public class PhoneBook1 {
    public static void main(String[] args) throws IOException {
     
        BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter wr=new BufferedWriter(new OutputStreamWriter(System.out));
        int n= Integer.parseInt(read.readLine());
        HashMap m=new HashMap();
        String s="";
        
        for(int i=0;i<n;i++){
            s=read.readLine();
            String s1[]=s.split(" ");
            if(s1[0].equals("add")){
                m.put(s1[1],s1[2]);
            }
            else if(s1[0].equals("find")){             
              Object ans= m.get(s1[1]);   
              if(ans==null){
                  wr.write("not found\n");
              }
              else{
                  wr.write(ans.toString()+"\n");
              }            
            }
            
            else{
               m.remove(s1[1]);
            }
            
            
        }
    wr.flush();
    }    
}
