/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;


class Node{
    long key = Long.MAX_VALUE;
    int left =-1;
    int right =-1;
    int parent = -1;
    int visited = 0;
}

/**
 *
 * @author Vaibhav
 */
public class IsBST {
    static Node[] node=null;
   static BufferedWriter  wr=new BufferedWriter(new OutputStreamWriter(System.out)); 
    public static void main(String[] args) throws Exception {
      int flag=0;  
       BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader read=new BufferedReader(new FileReader("D:\\Algorithms\\Week6\\tree_orders\\tests\\21"));
        int n=Integer.parseInt(read.readLine());
        node =new Node[n];
        String s="";
        String[] s1=null;
        long arr1[]=new long[n];
        for(int i=0 ; i < n ;i ++){
            node[i] =new Node();
        }
        
        
        for(int i = 0 ; i < n ; i ++){

            s=read.readLine();
            s1=s.split(" ");
            arr1[i]=Long.parseLong(s1[0]);
            node[i].key=Long.parseLong(s1[0]);
            node[i].left=Integer.parseInt(s1[1]);
            node[i].right=Integer.parseInt(s1[2]);
            if(node[i].left!=-1)
            node[node[i].left].parent=i;
            if(node[i].right!=-1)
            node[node[i].right].parent=i;
        }
        
        for(int i=0 ;i<n;i++){
            if(node[i].left!=-1){
                if(node[i].key==node[node[i].left].key){
                    System.out.println("INCORRECT");
                    flag=1;
                    break;
                }
            }
        }
        
        
    //    System.out.println("Start");
        if(flag==0)
        Arrays.sort(arr1);
        
        ArrayList<Long> list =  printInorder(0);
      long[] arr2 =new long[n];
      
      for(int i=0;i<list.size();i++){
          arr2[i]=list.get(i);
      }
      if(flag==0)
       for(int i=0;i<list.size();i++){
           if(arr1[i]!=arr2[i]){
               flag=1;
               System.out.println("INCORRECT");
               break;
           }
       }
       
       if(flag==0){
           System.out.println("CORRECT");
       }
    }
    
    static ArrayList<Long> printInorder(int i) throws Exception{
        ArrayList<Long> list =new ArrayList<Long>();
        try{
        while(true){
            if(i==0  && node[i].left!=-1 && node[node[i].left].visited== 1 && 
                    node[i].right!=-1 && node[node[i].right].visited== 1){     
                break;
            }
            else{
                if(node[i].left != -1 && node[node[i].left].visited == 0){ 
                    i= node[i].left;               
                     continue;
                }
                
                else if(node[i].left != -1 && node[node[i].left].visited==0){
                    i=node[i].left;
                }
                
                else if(node[i].left == -1 && node[i].visited==0){
                    list.add(node[i].key);
                    node[i].visited=1;
                    continue;
                }
                else if(node[i].left != -1 && node[node[i].left].visited==1 && node[i].visited==0){
                    list.add(node[i].key);
                    node[i].visited=1;
                }
                else if(node[i].visited==1 && node[i].right!=-1 && node[node[i].right].visited==0){
                    i=node[i].right;
                }
                else{
                    if(node[i].parent!=-1)
                    i=node[i].parent;
                    else{
                        break;
                    }
                }
                
            }
       
                }} catch(Exception e){
                    
                }
        return list;
    }
    
//    static void printpreorder(int i) throws IOException{
//       while(true){
//           if(node[i].visited==0){
//               System.out.print(node[i].key+" ");
//               node[i].visited=1;
//               continue;
//           }
//           else if(node[i].left!=-1 && node[node[i].left].visited==0){
//               i=node[i].left;
//               continue;
//           }
//           else if(node[i].right!=-1 && node[node[i].right].visited==0){
//               i=node[i].right;
//               continue;
//           }
//           else{
//               if(node[i].parent!=-1){
//               i=node[i].parent;
//               }
//               else{
//                   break;
//               }
//           }
//       }
//    }
//    
//    static void printpostorder(int i) throws IOException{
//        while(true){
//           if(node[i].left==-1 && node[i].right==-1 && node[i].visited==0 ||(node[i].left==-1  && node[i].right!=-1 && node[node[i].right].visited==1) || (node[i].left!=-1 && node[node[i].left].visited==1 && node[i].right==-1 ) ||(node[i].left!=-1 && node[node[i].left].visited==1 && node[i].right!=-1 && node[node[i].right].visited==1 )){
//               System.out.print(node[i].key+" ");
//               node[i].visited=1;
//               if(node[i].parent!=-1){
//                   i=node[i].parent;
//                   continue;
//               }
//               else{
//                   break;
//               }
//           }
//           else if(node[i].left!=-1 && node[node[i].left].visited==0){
//               i=node[i].left;
//           //    System.out.println("ehere");
//               continue;
//           }
//           else if(node[i].right!=-1 && node[node[i].right].visited==0){
//               i=node[i].right;
//             //  System.out.println("there");
//               continue;
//           }
//           
//           else{
//               
//           }
//           
//       }
//    }
    
 
 
    
    
}
