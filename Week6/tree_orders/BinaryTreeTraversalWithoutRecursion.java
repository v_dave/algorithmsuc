/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


class Node{
    long key = Integer.MAX_VALUE;
    int left =-1;
    int right =-1;
    int parent = -1;
    int visited = 0;
}

/**
 *
 * @author Vaibhav
 */
public class BinaryTreeTraversalWithoutRecursion {
    static Node[] node=null;
   static BufferedWriter  wr=new BufferedWriter(new OutputStreamWriter(System.out)); 
    public static void main(String[] args) throws Exception {
        
       BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader read=new BufferedReader(new FileReader("D:\\Algorithms\\Week6\\tree_orders\\tests\\21"));
        int n=Integer.parseInt(read.readLine());
        node =new Node[n];
        String s="";
        String[] s1=null;
        
        for(int i=0 ; i < n ;i ++){
            node[i] =new Node();
        }
        
        
        for(int i = 0 ; i < n ; i ++){

            s=read.readLine();
            s1=s.split(" ");
            node[i].key=Long.parseLong(s1[0]);
            node[i].left=Integer.parseInt(s1[1]);
            node[i].right=Integer.parseInt(s1[2]);
            if(node[i].left!=-1)
            node[node[i].left].parent=i;
            if(node[i].right!=-1)
            node[node[i].right].parent=i;
        }
        
    //    System.out.println("Start");
        printInorder(0);
        for(int i = 0 ; i < n ; i ++){
           node[i].visited=0;
        }
        
        System.out.println("");
        printpreorder(0);
        for(int i = 0 ; i < n ; i ++){
           node[i].visited=0;
        }
        System.out.println("");
        printpostorder(0);
    //    wr.write("\n");
        wr.flush();
    }
    
    static void printInorder(int i) throws Exception{
        
        
        while(true){
            if(node[i].parent==-1 && node[i].left!=-1 && node[node[i].left].visited== 1 && 
                    node[i].right!=-1 && node[node[i].right].visited== 1){     
                break;
            }
            else{
                if(node[i].left != -1 && node[node[i].left].visited == 0){ 
                    i= node[i].left;               
                     continue;
                }
                
                else if(node[i].left != -1 && node[node[i].left].visited==0){
                    i=node[i].left;
                }
                
                else if(node[i].left == -1 && node[i].visited==0){
                    System.out.print(node[i].key+" ");
                    node[i].visited=1;
                    continue;
                }
                else if(node[i].left != -1 && node[node[i].left].visited==1 && node[i].visited==0){
                    System.out.print(node[i].key+" ");
                    node[i].visited=1;
                }
                else if(node[i].visited==1 && node[i].right!=-1 && node[node[i].right].visited==0){
                    i=node[i].right;
                }
                else{
                    if(node[i].parent!=-1)
                    i=node[i].parent;
                    else{
                        break;
                    }
                }
                
            }
       
                }
}
    
    static void printpreorder(int i) throws IOException{
       while(true){
           if(node[i].visited==0){
               System.out.print(node[i].key+" ");
               node[i].visited=1;
               continue;
           }
           else if(node[i].left!=-1 && node[node[i].left].visited==0){
               i=node[i].left;
               continue;
           }
           else if(node[i].right!=-1 && node[node[i].right].visited==0){
               i=node[i].right;
               continue;
           }
           else{
               if(node[i].parent!=-1){
               i=node[i].parent;
               }
               else{
                   break;
               }
           }
       }
    }
    
    static void printpostorder(int i) throws IOException{
        while(true){
           if(node[i].left==-1 && node[i].right==-1 && node[i].visited==0 ||(node[i].left==-1  && node[i].right!=-1 && node[node[i].right].visited==1) || (node[i].left!=-1 && node[node[i].left].visited==1 && node[i].right==-1 ) ||(node[i].left!=-1 && node[node[i].left].visited==1 && node[i].right!=-1 && node[node[i].right].visited==1 )){
               System.out.print(node[i].key+" ");
               node[i].visited=1;
               if(node[i].parent!=-1){
                   i=node[i].parent;
                   continue;
               }
               else{
                   break;
               }
           }
           else if(node[i].left!=-1 && node[node[i].left].visited==0){
               i=node[i].left;
           //    System.out.println("ehere");
               continue;
           }
           else if(node[i].right!=-1 && node[node[i].right].visited==0){
               i=node[i].right;
             //  System.out.println("there");
               continue;
           }
           
           else{
               
           }
           
       }
    }
    
 
 
    
    
}
