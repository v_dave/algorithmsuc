import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class BuildHeap2 {
    
    static StringBuilder sb=null;
    static long[] arr=null;
    static int counter=0;
    public static void main(String[] args) throws Exception {
       // BufferedReader read=new BufferedReader(new FileReader("D:\\Algorithms\\make_heap\\tests\\04"));
        BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter wr=new BufferedWriter(new OutputStreamWriter(System.out));
        
        
        int n=Integer.parseInt(read.readLine());
        int n2= (int) n/2;
        String s=read.readLine();
        String[] s1=s.split(" ");
        
        sb = new StringBuilder("");
        arr=new long[s1.length];
        
        for(int i=0;i<arr.length;i++){
            arr[i] = Long.parseLong(s1[i]);
        }
        
        
        
        for(int i=n/2 ;i>=0; --i){          
            siftdown(i);
        }
        
            
        System.out.println(counter);
        
        s1=sb.toString().split(";");
        
        
        for(int i=0;i<s1.length && !s1[i].equals("");i++){
            wr.write(s1[i]+"\n");
        }
        
        wr.flush();
      //  System.out.println(Arrays.toString(arr));
    }
    
    
    
    
    static void siftdown(int index){
        
        int l_node= 2*index+1 ;
        int minindex = index;
        
        if(arr.length > l_node && arr[l_node] < arr[index] ){
            minindex = l_node;
        }
        
        int r_node = 2*index+2;
        
        if(arr.length > r_node && arr[r_node] < arr[minindex] ){
            minindex = r_node;
        }
        
        if(index!=minindex){
            sb.append(index+" "+minindex+";");
            long temp= arr[index] ;
            arr[index]=arr[minindex];
            arr[minindex]=temp;
            counter++;
            siftdown(minindex);
        }
    }
}
