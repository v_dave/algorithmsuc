import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Stack;

class Bracket {
    Bracket(char type, int position) {
        this.type = type;
        this.position = position;
    }

    boolean Match(char c) {
        if (this.type == '[' && c == ']')
            return true;
        if (this.type == '{' && c == '}')
            return true;
        if (this.type == '(' && c == ')')
            return true;
        return false;
    }

    char type;
    int position;
}

class Assignments {
    public static void main(String[] args) throws IOException {
        InputStreamReader input_stream = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input_stream);
        String text = reader.readLine();
        int flag=0;
        Stack<Bracket> opening_brackets_stack = new Stack<Bracket>();
        for (int position = 0; position < text.length(); ++position) {
            char next = text.charAt(position);

            if (next == '(' || next == '[' || next == '{') {
                opening_brackets_stack.push(new Bracket(next,position));
            }

            if (next == ')' || next == ']' || next == '}') {
                
                if(opening_brackets_stack.isEmpty()){
                    System.out.println((position+1));
                    flag=1;
                    break;
                }
                
                else{
                 Bracket temp = opening_brackets_stack.peek();
                
                if(temp.Match(next)){
                    opening_brackets_stack.pop();
                }
                else{
                    System.out.println((position+1));
                    flag=1;
                    break;
                }
                }
            }
        }
        
        if(flag==0){
        if(!opening_brackets_stack.empty()){
            Bracket temp= opening_brackets_stack.pop();
            System.out.println((temp.position+1));
        }
        else{
            System.out.println("Success");
        }
        }
        // Printing answer, write your code here
    }
}
