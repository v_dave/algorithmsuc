import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class Job {
    
    static int arr[]=null;
    static int threads[]=null;
    static int pointer=0;
    static StringBuilder sb=null;
    static long counter=0;
    
    
    public static void main(String[] args) throws Exception {
      //  BufferedReader read=new BufferedReader(new FileReader("D:\\Algorithms\\job_queue\\tests\\08"));
        BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter wr=new BufferedWriter(new OutputStreamWriter(System.out));
        
        String s=read.readLine();
        int n = Integer.parseInt(s.split(" ")[0]);
        int m = Integer.parseInt(s.split(" ")[1]);
        
        s=read.readLine();
        
        String s1[]=s.split(" ");
        
            arr=new int[s1.length];
            Arrays.fill(arr, 0);
        
        for(int i=0 ;i<arr.length;i++ ){
            arr[i]=Integer.parseInt(s1[i]);
        }
        
         threads =new int[n];
         sb=new StringBuilder("");
         
         for(int i=0; i < arr.length && i < n;i++){
             threads[i]=arr[i];
             sb.append(i+" 0"+";");
         }
         
         
         pointer=n;
         
         if(pointer<arr.length){
            for(long i=0;;i++){
               int x = checker();
               if(x==-1){
                   break;
               }
            }
            }
         
         s1=sb.toString().split(";");
         
         for(int i=0;i<s1.length;i++){
             wr.write(s1[i]+"\n");
         }
         
         wr.flush();
    }
    
    static int checker(){
        int minindex=-1;
        int min= Integer.MAX_VALUE;
        for(int i=0;i<threads.length;i++){
            if(threads[i]<min){
                minindex=i;
                min=threads[i];
            }
        }
        for(int i=0;i<threads.length;i++){
            threads[i]=threads[i]-min;
        }
        
        counter=counter+min;
        sb.append(minindex+" "+counter+";");
        
        threads[minindex]=arr[pointer];
        pointer++;
        
        if(pointer>=(arr.length)){
            return -1;
        }
        
    return 1;
    }
    
    
}
